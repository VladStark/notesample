package com.example.victor.notesample;

import android.content.SharedPreferences;
import android.os.PersistableBundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.Date;

public class EditorActivity extends AppCompatActivity {

    public static final String INTENT_KEY_NOTE_ID = "note_id";
    private DBHelper mDBHelper;
    private Note mNote;
    private FloatingActionButton mBadNote;

    private EditText mETTitle, mETDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);

        mETDescription = (EditText) findViewById(R.id.tv_note_text);
        mETTitle = (EditText) findViewById(R.id.tv_title);
        mBadNote = (FloatingActionButton) findViewById(R.id.b_add_note);
        mBadNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = mETTitle.getText().toString();
                String noteText = mETDescription.getText().toString();
                Long date = new Date().getTime();

                mDBHelper.insertNewNote(title,noteText,date);
            }
        });
        mDBHelper = new DBHelper(this);
        int id = getIntent().getExtras().getInt(INTENT_KEY_NOTE_ID);
        if (id > 0) {
            Note note = mDBHelper.getNoteById(id);

            mETTitle.setText(note.getTitle());
            mETDescription.setText(note.getDescription());
        }
    }


    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Новая заметка");
            setTitle("Новая заметка");
        }
    }
}
