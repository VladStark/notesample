package com.example.victor.notesample;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by victor on 18.02.17.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Notes.db";

    public static final String TABLE_NAME = "notes";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_DESCRIPTION = "description";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME
                +"("
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_TITLE + " TEXT,"
                + COLUMN_DATE + " INTEGER,"
                + COLUMN_DESCRIPTION + " TEXT" + ");");

        long date = new Date().getTime();
        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + COLUMN_TITLE + ", " + COLUMN_DATE + ", " + COLUMN_DESCRIPTION + ")"
                + " VALUES (\"Добро пожаловать\", " + date + ",\"Тут можно писать заметки\");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }


    public ArrayList<Note> getAllNotes () {
        SQLiteDatabase database = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME + ";";
        Cursor cursor = database.rawQuery(query, null);
        ArrayList<Note> notes = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Note note = new Note();
                note.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_ID))));
                note.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                note.setDate(Long.valueOf(cursor.getString(cursor.getColumnIndex(COLUMN_DATE))));
                note.setDescription(cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)));
                notes.add(note);
            } while (cursor.moveToNext());
        }
        return notes;
    }


    public Note getNoteById(int id) {

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_ID + "=" + id +";",null);
        cursor.moveToFirst();
        Note note = new Note();
        if(cursor != null){
            note.setId(id);
            note.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
            note.setDate(Long.valueOf(cursor.getString(cursor.getColumnIndex(COLUMN_DATE))));
            note.setDescription(cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)));
        }
        return note;
    }

    public void insertNewNote(String title, String noteText, Long date) {
        SQLiteDatabase db = getReadableDatabase();
        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + COLUMN_TITLE + ", " + COLUMN_DATE + ", " + COLUMN_DESCRIPTION + ")"
                + " VALUES (\""+ title + "\"," + date + "," + "\"" + noteText + "\");");
    }
}
